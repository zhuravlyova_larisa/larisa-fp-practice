module Main where
 
type Context = [(String, Binding)]
 
data Binding = NameBind
	     | VarBind Type
 
add_binding :: Context -> String -> Binding -> Context
 
add_binding ctx x bind = (x, bind):ctx
 
get_binding :: Context -> Int -> Binding
 
get_binding ctx index | index > length ctx = error "requested index higher than context"
                      | otherwise = snd $ ctx !! (length ctx - index - 1)
 
data Type = BooleanType
	  | ArrType Type Type
	  deriving (Show,Eq)
 
data Term = Var Int Int
		  | Lambda String Type Term
		  | App Term Term
		  | TermTrue
		  | TermFalse
		  | TermIf Term Term Term
 
 
get_type :: Term -> Type
get_type term = typeof term []
 
typeof :: Term -> Context -> Type
typeof term ctx = case term of
	TermTrue -> BooleanType
	TermFalse -> BooleanType
	TermIf term1 term2 term3 ->
		if (typeof term1 ctx) == BooleanType then
			let term2Type = typeof term2 ctx in
			if term2Type == typeof term3 ctx  then term2Type
				else error "arms of conditional have different types"
		else error "guard of conditional not a boolean"
	Var index _ ->
	   let bind = get_binding ctx index in
	   case bind of
	       NameBind -> error "no name bindings"
	       VarBind typ -> typ
	Lambda arg argType t ->
	    let ctx' = add_binding ctx arg (VarBind(argType)) in
		let bodyType = typeof t ctx' in
		ArrType argType bodyType
	App t1 t2 ->
		let t1Type = (typeof t1 ctx) in
		let t2Type = (typeof t2 ctx) in
		case t1Type of
			ArrType t11Type t12Type ->
				if t2Type == t11Type
					then t12Type
					else error "type mismatch in App"
			_ -> error "App must be of an array type"
 
 
term0 = TermFalse
 
-- BooleanType
term1 = TermIf TermFalse (App (Lambda "s" BooleanType TermFalse) (TermFalse)) TermTrue
 
-- ArrType
term2 = Lambda "s" BooleanType TermFalse 

-- | The main entry point.
main :: IO ()
main = do
    print $ get_type term0
    print $ get_type term1
    print $ get_type term2