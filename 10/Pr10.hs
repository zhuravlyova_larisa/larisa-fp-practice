
import Control.Applicative

{-

Распознать язык

((\x . x) (\x y . y))

-}

data Term = Var String
          | Lam String Term
          | App Term Term
            deriving (Eq, Show)

type ErrorMsg = String
newtype Parser a = Parser { runParser :: String -> Either ErrorMsg [(a, String)] }

instance Functor Parser where
    fmap f (Parser g) = Parser $ \s -> case g s of
                                         Left err -> Left err
                                         Right values -> Right $ map (\(a,rest) -> (f a, rest)) values

instance Applicative Parser where
    pure f = Parser $ \s -> Right [(f,s)]
    Parser f <*> Parser a = Parser $ \s -> case f s of
                                             Left err -> Left err
                                             Right funs -> Right $ concatMap (\(fun,rest) -> apply fun rest a) funs
                                                 where apply fun rest a = case a rest of
                                                                            Left err' -> []
                                                                            Right vals -> map (\(val, rest') -> (fun val, rest')) vals

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
                           [] -> Left "No chars left"
                           (c:cs) -> Right [(c,cs)]

char :: Char -> Parser Char
char c = Parser $ \s -> case s of
                          [] -> Left "No chars left"
                          (c':cs) -> if c /= c'
                                     then Left $ [c] ++ " was expected, " ++ [c'] ++ " was found"
                                     else Right [(c',cs)]

satisfy :: (Char -> Bool) -> Parser Char
satisfy f = Parser $ \s -> case s of
                             [] -> Left "No chars left"
                             (c:cs) -> if f c
                                       then Right [(c,cs)]
                                       else Left "Not satisfied"

instance Alternative Parser where
    empty = Parser $ \s -> Right []
    Parser f <|> Parser g = Parser $ \s -> case f s of
                                             Left err -> g s
                                             Right valRest -> Right valRest

-- many elem = empty <|> (:) <$> elem <*> many elem
                                                              
letter = satisfy (`elem`(['a'..'z'] ++ ['A'..'Z']))
digit = satisfy (`elem`['0'..'9'])
                                                              
var :: Parser String
var = (:) <$> letter <*> many (letter <|> digit)
-- (<$>) f a = pure f <*> a

spaces :: Parser String
spaces = many $ char ' '

bracketed :: Parser a -> Parser a
bracketed parser = char '(' *> parser <* char ')'

lam :: Parser Term
lam = char '\\' *> (Lam <$> var <* spaces <* char '.' <* spaces <*> term)

app :: Parser Term
app = App <$> term <* spaces <*> term 

term :: Parser Term
term = (Var <$> var)
   <|> bracketed (lam
              <|> app)
   
