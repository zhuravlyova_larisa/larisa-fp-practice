module Main where

import  Data.List

data Expr = Var String
          | App Expr Expr
          | Lam String Expr
          deriving (Show)

freeVars :: Expr -> [String]
freeVars (Var s) = [s]
freeVars (App a b) = freeVars a `union` freeVars b
freeVars (Lam c d) = freeVars d \\ [c]

replacement :: String -> Expr -> Expr -> Expr
replacement v x b = replacement' b
  where replacement' (Var i) = if i == v then x else Var i
        replacement' (App f a) = App (replacement' f) (replacement' a)
        replacement' (Lam i e) =
            if v == i then
                Lam i e
            else if i `elem` vars then
                let i' = checkIn e i
                    e' = replacementVar i i' e
                in  Lam i' (replacement' e')
            else
                Lam i (replacement' e)
        vars = freeVars x
        checkIn e i = helper i
           where helper i' = if i' `elem` vars' then helper (i ++ "'") else i'
                 vars' = vars ++ freeVars e

replacementVar :: String -> String -> Expr -> Expr
replacementVar s s' e = replacement s (Var s') e

exprEqual :: Expr -> Expr -> Bool
exprEqual (Var v)   (Var v')    = v == v'
exprEqual (App f a) (App f' a') = exprEqual f f' && exprEqual a a'
exprEqual (Lam s e) (Lam s' e') = exprEqual e (replacementVar s' s e')
exprEqual _ _ = False

betaEq :: Expr -> Expr -> Bool
betaEq e1 e2 = exprEqual (normalForms e1) (normalForms e2)

normalForms :: Expr -> Expr
normalForms ee = helper ee []
  where helper (App f a) as = helper f (a:as)
        helper (Lam s e) [] = Lam s (normalForms e)
        helper (Lam s e) (a:as) = helper (replacement s a e) as
        helper f as = app f as
        app f as = foldl App f (map normalForms as)

-- (λx.λx.x)(λx.x)
t1 = App (Lam "x" $ Lam "x" $ Var "x") (Lam "x" $ Var "x")
-- (λx.λx.x)(λx.λy.x)
t2 = App (Lam "x" $ Lam "x" $ Var "x") (Lam "x" $ Lam "y" $ Var "x")

[z,s,m,n] = map (Var . (:[])) "zsmn" -- [Var "z", Var "s", Var "m", Var "n"]
appl func x y = App (App func x) y

one   = Lam "s" $ Lam "z" $ App s z
two   = Lam "s" $ Lam "z" $ App s $ App s z
three = Lam "s" $ Lam "z" $ App s $ App s $ App s z
plus  = Lam "m" $ Lam "n" $ Lam "s" $ Lam "z" $ appl m s (appl n s z)

t = Lam "y" $ Lam "z" $ Lam "x" $ Var "x"
tid = Lam "x" $ Var "x"

tcase2 = Lam "x" $ Lam "y" $ Lam "z" $ Var "x"
tcase1 = App tcase2 tid

main = do
    print $ normalForms tcase1
    putStr "(Lx.Lx.x)(Lx.x) -> ... -> "
    print $ normalForms t1
    putStr "(Lx.Lx.x)(Lx.Ly.x) -> ... -> "
    print $ normalForms t2
    putStr "Is 1 + 1 equal to 2 ? -> ... -> "
    print (if betaEq (appl plus one one) two then "YES" else "NO")
    putStr "Is 1 + 2 equal to 3 ? -> ... -> "
    print (if betaEq (appl plus one two) three then "YES" else "NO")
    putStr "Is 2 + 3 == 3 + 2 -> ... -> "
    print $ if betaEq (appl plus two three) (appl plus three two) then "YES" else "NO"