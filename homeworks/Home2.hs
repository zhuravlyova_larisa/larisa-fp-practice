-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]

deleteInt n [] = []
deleteInt n (x:xs) | n == x = deleteInt n xs
                 | otherwise = (x : (deleteInt n xs)) 
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesIntInternal :: Int -> Int -> [Int] -> [Int]
findIndicesIntInternal index n [] = []
findIndicesIntInternal index n (x:xs) | n == x = index : (findIndicesIntInternal (index+1) n xs)
                                      | otherwise = findIndicesIntInternal (index+1) n xs

findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt = findIndicesIntInternal 0


